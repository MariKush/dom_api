var characters = [];
var charactersToAdd = [];
var url;
let lock;
let strComparator = new Intl.Collator();

let deleted = [];

function constructor() {
    characters = [];
    charactersToAdd = [];
    url = 'https://rickandmortyapi.com/api/character';
    lock = false;

    document.getElementById("selectSort").selectedIndex = 0;
    document.getElementById("countEpisods").checked = false;

}

window.onload = function () {
    constructor();
    getNext();
}

function checkScroll() {
    if (lock) return;
    if (document.getElementById("search").value != "") return;
    let a = document.documentElement.scrollTop;
    let b = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    if (b - a <= 200) {
        lock = true;
        getNext();
    }
}
window.onscroll = function () {
    checkScroll()
};

function getNext() {
    if (charactersToAdd.length >= 10) {
        showNextPage()
        lock = false;
    } else if (url != null) {
        fetch(url)
            .then(res => res.json())
            .then(data => {
                url = data.info.next;
                const rawData = data.results;
                rawData.map(_character => {
                    let created = _character.created.substring(0, 10);
                    let species = _character.species;
                    let img = _character.image;
                    let episodes = _character.episode;
                    let name = _character.name;
                    let location = _character.location;
                    let id = _character.id;

                    if (!deleted.includes(id)) {
                        let character = new Character(created, species, img, episodes, name, location, id);
                        charactersToAdd.push(character);
                    }

                });

                showNextPage();
                lock = false;
            })
            .catch((error) => {
                console.log(error);
            })
    }

}

function showNextPage() {
    let returnCharacters = charactersToAdd.slice(0, 10);
    charactersToAdd = charactersToAdd.slice(10);

    characters = characters.concat(returnCharacters);
    if (isSorting()) {
        displayWithSorting();
    } else {
        display(returnCharacters);
    }
}

function displayWithSorting() {
    clear();
    sort();
    display(characters);
}

function clear() {
    document.getElementById("characters").innerHTML = '';
}

function display(characters) {
    characters.forEach(element => {
        add(element)
    });
}

function reset() {
    clear();
    constructor();
    getNext();
}

function remove(id) {
    document.getElementById(id).remove();
    deleted.push(id);
    characters = characters.filter(el => el.id != id);
    checkScroll();
}

function add(character) {
    let container = document.getElementById("characters");
    let newCard = document.createElement("div");
    newCard.id = character.id;
    container.appendChild(newCard);
    newCard.classList = "post";


    let name = document.createElement("h3");
    name.innerHTML = character.name;
    name.className = 'characterName';
    newCard.appendChild(name);

    let crissСross = document.createElement("button");
    crissСross.innerHTML = '&#x2716;'
    crissСross.className = 'deleteButton'
    crissСross.onclick = function () {
        remove(character.id)
    };
    newCard.appendChild(crissСross);


    let image = document.createElement("img");
    image.src = character.img;
    image.classList = "post-img";
    newCard.appendChild(image);

    let species = document.createElement("div");
    species.innerHTML = "species: " + character.species;
    newCard.appendChild(species);

    let location = document.createElement("div");
    location.innerHTML = "location: " + character.location.name;
    newCard.appendChild(location);

    let created = document.createElement("div");
    created.innerHTML = "created: " + character.created;
    newCard.appendChild(created);

    let episode = document.createElement("div");
    episode.innerHTML = "count episods: " + character.episodes.length;
    newCard.appendChild(episode);

}

function getSortByDate() {
    let e = document.getElementById("selectSort");
    return e.options[e.selectedIndex].value;
}

function getSortByEpisods() {
    return document.getElementById("countEpisods").checked;
}

function isSorting() {
    return getSortByEpisods() || getSortByDate() != 'none';
}

function sort() {
    characters.sort(compare)
}

function compare(characterA, characterB) {
    if (getSortByEpisods()) {
        if (characterA.episodes.length != characterB.episodes.length)
            return characterB.episodes.length - characterA.episodes.length;
    }

    if (getSortByDate() != 'none') {
        console.log(getSortByDate());
        console.log(characterA);
        console.log(characterB);
        if (getSortByDate() == 'asc') {
            return new Intl.Collator().compare(characterA.created, characterB.created);
        } else {
            return new Intl.Collator().compare(characterB.created, characterA.created);
        }
    }
    return 0;
}

function search(text) {
    console.log(text);
    let arr = characters.slice(0);
    arr = arr.filter(el => el.name.toLowerCase().includes(text.toLowerCase()));
    clear();
    display(arr);
}