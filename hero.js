class Character {
    constructor(created, species, img, episodes, name, location, id) {
        this.id=id;
        this.created = created;
        this.species = species;
        this.img = img;
        this.episodes = episodes;
        this.name = name;
        this.location = location;
      }
}